export default {
  // eslint-disable-next-line no-unused-vars
  install: (app, options) => {
    app.mixin({
      data () {
        return {
          breakpoints: {
            xs: undefined,
            sm: undefined,
            md: undefined,
            lg: undefined,
            xl: undefined
          },
          activeMediaBreakpoint: "xl"
        };
      },
      created () {
        const style = getComputedStyle(document.body);

        Object.keys(this.breakpoints).forEach((key) => {
          this.breakpoints[key] = Number(style.getPropertyValue("--breakpoint-" + key).replace("px", ""));
        });
      },
      mounted () {
        this.onResize();
        window.addEventListener("resize", this.onResize);
      },
      methods: {
        onResize () {
          const innerWidth = window.innerWidth,
            sortedBreakpoints = Object.entries(this.breakpoints).sort(([, widthA], [, widthB]) => {
              return widthA > widthB ? 1 : -1;
            }),
            activeBreakpoint = sortedBreakpoints.find(([, width], index) => {
              const nextBreakpoint = Object.entries(this.breakpoints)[index + 1];

              return innerWidth >= width && (nextBreakpoint !== undefined && innerWidth < nextBreakpoint[1] || nextBreakpoint === undefined);
            });

          this.activeMediaBreakpoint = activeBreakpoint[0];
        }
      }
    });
  }
};
