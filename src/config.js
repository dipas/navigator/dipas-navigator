/**
 * @license GPL-2.0-or-later
 */

let DevConfig;

try {
  const DevConfigImport = require("./config.local.js");

  DevConfig = DevConfigImport.DevConfig;
}
catch (e) {
  DevConfig = {};
}

/* eslint-disable no-unused-vars */
export const Config = Object.assign({
  backend: {
    useDifferentBackendHost: false,
    useSSL: false,
    hostname: "",
    port: 80,
    basepath: "/drupal/"
  },
  locale: "de" // Language Settings for Frontend - Set "en" for english
}, DevConfig);
