import {Config} from "../config.js";
import {createStore} from "vuex";
import moment from "moment";

export default createStore({
  state: {
    basePath: "",
    projectlist: [],
    appointmentlist: [],
    documentations: [],
    proceedingGeoJson: {},
    contributions: [],
    comments: [],
    recentContributions: [],
    mostCommentedContributions: [],
    contentPages: {}
  },
  actions: {
    /**
     * Perform http request and return Promise
     * @param   {this} context this store
     * @param   {object} request request
     * @returns {Promise} result of http request or error
     */
    doRequest (context, request) {
      const requestPath = [];

      if (Config.backend.useDifferentBackendHost) {
        requestPath.push(Config.backend.useSSL ? "https://" : "http://");
        requestPath.push(Config.backend.hostname);
        if (!Config.backend.useSSL && Config.backend.port !== 80 || Config.backend.useSSL && Config.backend.port !== 443) {
          requestPath.push(":" + Config.backend.port);
        }
      }
      requestPath.push(Config.backend.basepath);

      context.dispatch("basePath", requestPath);

      return new Promise((resolve, reject) => {
        fetch(requestPath.join("") + request.endpoint)
          .then(response => response.json())
          .then(json => resolve(json))
          .catch(error => reject(error));
      });
    },
    projectlist ({dispatch, commit}) {
      dispatch("doRequest", {
        endpoint: "dipas-pds/projects"
      }).then(FeatureCollection => {
        commit("projectlist", FeatureCollection.features);
      });
    },
    appointmentlist ({dispatch, commit}) {
      dispatch("doRequest", {
        endpoint: "dipas/navigator/appointments"
      }).then(response => {
        commit("appointmentlist", response.appointments);
      });
    },
    documentations ({dispatch, commit}) {
      dispatch("doRequest", {
        endpoint: "dipas/navigator/documentations"
      }).then(response => {
        commit("documentations", response.files);
      });
    },
    proceedingGeoJson ({dispatch, commit}) {
      dispatch("doRequest", {
        endpoint: "dipas/navigator/cockpitdatamap"
      }).then(response => {
        commit("proceedingGeoJson", response);
      });
    },
    contributions ({dispatch, commit}) {
      dispatch("doRequest", {
        endpoint: "dipas/navigator/contributions"
      }).then(response => {
        commit("contributions", response);
      });
    },
    comments ({dispatch, commit}) {
      dispatch("doRequest", {
        endpoint: "dipas/navigator/comments"
      }).then(response => {
        commit("comments", response);
      });
    },
    basePath ({commit}, baseUrl) {
      commit("basePath", baseUrl);
    },
    recentContributions ({dispatch, commit}) {
      dispatch("doRequest", {
        endpoint: "dipas/navigator/recentcontributions"
      }).then(response => {
        commit("recentContributions", response);
      });
    },
    mostCommentedContributions ({dispatch, commit}) {
      dispatch("doRequest", {
        endpoint: "dipas/navigator/mostcommentedcontributions"
      }).then(response => {
        commit("mostCommentedContributions", response);
      });
    },
    contentPage ({state, dispatch, commit}, page) {
      if (!state.contentPages[page]) {
        dispatch("doRequest", {
          endpoint: "dipas/navigator/page/" + page
        }).then(response => {
          commit("contentPage", {
            page,
            content: response
          });
        });
      }
    }
  },
  mutations: {
    basePath (state, basePath) {
      state.basePath = basePath.join("");
    },
    projectlist (state, projects) {
      state.projectlist = projects;
    },
    appointmentlist (state, appointments) {
      state.appointmentlist = appointments;
    },
    documentations (state, files) {
      state.documentations = files;
    },
    proceedingGeoJson (state, geoJson) {
      state.proceedingGeoJson = geoJson;
    },
    contributions (state, contributions) {
      state.contributions = contributions;
    },
    comments (state, comments) {
      state.comments = comments;
    },
    recentContributions (state, recentContributions) {
      state.recentContributions = recentContributions.contributions;
    },
    mostCommentedContributions (state, mostCommentedContributions) {
      state.mostCommentedContributions = mostCommentedContributions.contributions;
    },
    contentPage (state, data) {
      let contentPages = JSON.parse(JSON.stringify(state.contentPages));

      contentPages[data.page] = data.content;
      state.contentPages = contentPages;
    }
  },
  getters: {
    basePath (state) {
      return state.basePath;
    },
    projectlist (state) {
      return state.projectlist;
    },
    appointmentlist (state) {
      return state.appointmentlist;
    },
    recentContributions (state) {
      return state.recentContributions;
    },
    mostCommentedContributions (state) {
      return state.mostCommentedContributions;
    },
    proceedingByYears (state, getters) {
      const proceedings = [...getters.projectlist],
        proceedingsByYears = {},
        sortedResponse = {};

      proceedings.forEach(proceeding => {
        const yearStart = moment(proceeding.properties.dateStart).format("Y"),
          yearEnd = moment(proceeding.properties.dateEnd).format("Y");

        if (proceedingsByYears[yearStart] === undefined) {
          proceedingsByYears[yearStart] = 0;
        }

        if (proceedingsByYears[yearEnd] === undefined) {
          proceedingsByYears[yearEnd] = 0;
        }

        proceedingsByYears[yearStart]++;
        if (yearStart !== yearEnd) {
          proceedingsByYears[yearEnd]++;
        }
      });

      Object.keys(proceedingsByYears).sort((a, b) => a > b ? 1 : -1).forEach(year => {
        sortedResponse[year] = proceedingsByYears[year];
      });

      return sortedResponse;
    },
    proceedingsByTopics (state, getters) {
      const proceedings = [...getters.projectlist],
        proceedingsByTopics = {},
        sortedResponse = [];

      proceedings.forEach(proceeding => {
        Object.values(proceeding.properties.projectTopics).map(topic => topic.name).forEach(topic => {
          if (proceedingsByTopics[topic] === undefined) {
            proceedingsByTopics[topic] = 0;
          }
          proceedingsByTopics[topic]++;
        });
      });

      // eslint-disable-next-line one-var
      const countTotal = Object.values(proceedingsByTopics).length
        ? Object.values(proceedingsByTopics).reduce((accumulated, current) => accumulated + current)
        : 0;

      Object.entries(proceedingsByTopics).forEach(([topic, count]) => {
        sortedResponse.push({
          topic,
          count,
          percentage: Math.ceil(100 * count / countTotal)
        });
      });

      return sortedResponse.sort((a, b) => a.count < b.count ? 1 : -1);
    },
    proceedingsByDistricts (state, getters) {
      const proceedings = [...getters.projectlist],
        districtColors = {},
        proceedingsByDistricts = {},
        sortedResponse = [];

      proceedings.forEach(proceeding => {
        Object.values(proceeding.properties.dipasMainDistrict).forEach(district => {
          districtColors[district.name] = district.field_color;
        });
      });

      proceedings.forEach(proceeding => {
        Object.values(proceeding.properties.dipasMainDistrict).map(district => district.name).forEach(district => {
          if (proceedingsByDistricts[district] === undefined) {
            proceedingsByDistricts[district] = 0;
          }
          proceedingsByDistricts[district]++;
        });
      });

      // eslint-disable-next-line one-var
      const countTotal = Object.values(proceedingsByDistricts).length
        ? Object.values(proceedingsByDistricts).reduce((accumulated, current) => accumulated + current)
        : 0;

      Object.entries(proceedingsByDistricts).forEach(([district, count]) => {
        sortedResponse.push({
          district,
          color: districtColors[district],
          count,
          percentage: Math.ceil(100 * count / countTotal)
        });
      });

      return sortedResponse.sort((a, b) => a.count < b.count ? 1 : -1);
    },
    documentations (state) {
      const arraySize = 3;

      return state.documentations.sort((a, b) => a.upload_date < b.upload_date ? 1 : -1).slice(0, arraySize);
    },
    proceedingGeoJson (state) {
      return state.proceedingGeoJson;
    },
    contributionsByYears (state) {
      const data = [];

      if (state.contributions.chart_data !== undefined) {
        Object.entries(state.contributions.chart_data.by_year).forEach(([year, contributions]) => {
          data.push({x: year, y: Number(contributions)});
        });
      }

      return data;
    },
    contributionsCount (state) {
      let count = 0;

      if (state.contributions.chart_data !== undefined) {
        count = state.contributions.chart_data.total;
      }

      return count;
    },
    maximumContributionsPerYear (state) {
      let count = 0;

      if (state.contributions.chart_data !== undefined) {
        count = state.contributions.chart_data.max;
      }

      return count;
    },
    commentsByYears (state) {
      const data = [];

      if (state.comments.chart_data !== undefined) {
        Object.entries(state.comments.chart_data.by_year).forEach(([year, comments]) => {
          data.push({x: year, y: Number(comments)});
        });
      }

      return data;
    },
    commentsCount (state) {
      let count = 0;

      if (state.comments.chart_data !== undefined) {
        count = state.comments.chart_data.total;
      }

      return count;
    },
    maximumCommentsPerYear (state) {
      let count = 0;

      if (state.comments.chart_data !== undefined) {
        count = state.comments.chart_data.max;
      }

      return count;
    },
    contentPage (state) {
      return (page) => {
        return state.contentPages[page]
          ? state.contentPages[page]
          : {};
      };
    }
  }
});
