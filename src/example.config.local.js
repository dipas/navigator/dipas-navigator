export const DevConfig = {
  backend: {
    useDifferentBackendHost: true,
    useSSL: false,
    hostname: "localhost",
    port: 8080,
    basepath: "/drupal/"
  }
};
