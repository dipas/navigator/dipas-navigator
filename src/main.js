import {createApp} from "vue";
import {createI18n} from "vue-i18n";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import bootstrapBreakpoints from "./plugins/bootstrapBreakpoints.js";
import {FunctionLibrary} from "./plugins/FunctionLibrary.js";

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "material-design-icons/iconfont/material-icons.css";

function loadLocaleMessages () {
  const locales = require.context("./locales", true, /[A-Za-z0-9-_,\s]+\.json$/i),
    messages = {};

  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i);

    if (matched && matched.length > 1) {

      const locale = matched[1];

      messages[locale] = locales(key);
    }
  });
  return messages;
}

const i18n = createI18n({
  legacy: false,
  // eslint-disable-next-line
  locale: LOCALE,
  globalInjection: true,
  messages: loadLocaleMessages()
});

createApp(App).use(store).use(router).use(i18n).use(bootstrapBreakpoints).use(FunctionLibrary).mount("#app");

